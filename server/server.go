package server

import (
	"bitbucket.org/satween/srvengine/auth"
	"bitbucket.org/satween/srvengine/db"
	"bitbucket.org/satween/srvengine/session"
	"bitbucket.org/satween/srvengine/verification"
	"log"
	"net/http"
)

var authConfig *AuthConfig = nil

type RequestHandler struct {
	Path        string
	HandlerFunc func(w http.ResponseWriter, r *http.Request)
}

type AuthConfig struct {
	Login        RequestHandler
	Logout       RequestHandler
	GoogleKey    string
	GoogleSecret string
}

type Configuration struct {
	Handlers []RequestHandler
	Key      string // optional
	Cert     string // optional
	Port     string
	Auth     *AuthConfig
}

func Run(config Configuration) {
	session.Setup()

	if config.Auth != nil {

		if db.Db == nil {
			panic("Db required for Auth module")
		}

		authConfig = config.Auth

		verification.IsNotEmpty(authConfig.Login.Path, "Login path cannot be empty. It should be something like \"/login\"")
		http.HandleFunc(authConfig.Login.Path, authConfig.Login.HandlerFunc)

		verification.IsNotEmpty(authConfig.Logout.Path, "Logout path cannot be empty. It should be something like \"/logout\"")
		http.HandleFunc(authConfig.Logout.Path, authConfig.Logout.HandlerFunc)

		verification.IsNotEmpty(config.Auth.GoogleKey, "GoogleKey is required for auth module")
		verification.IsNotEmpty(config.Auth.GoogleSecret, "GoogleSecret is required for auth module")

		auth.Init("http://localhost:"+config.Port+"/google_oauth_callback", authConfig.GoogleKey, authConfig.GoogleSecret)
		http.HandleFunc("/"+auth.GoogleOauthRedirectPath, auth.GoogleOAuthCallback)
	}

	for _, config := range config.Handlers {
		http.HandleFunc(config.Path, config.HandlerFunc)
	}

	http.Handle("/", http.FileServer(http.Dir("static")))

	if len(config.Cert) > 0 && len(config.Key) > 0 {
		runHttpsWithAutoRedirect(config.Port, config.Cert, config.Key)
	} else {
		runHttpServer(config.Port)
	}
}

func AuthorizationRequired(delegate func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if authConfig == nil {
			delegate(w, r)
		} else {
			s := session.GetSession()
			s.SetValue(r, session.KeyLastUnauthorizedUrl, schemeAndHost(r)+r.URL.Path)
			_ = s.Save(r, w)
			if s.GetProfile(r) == nil {
				redirectAuth(w, r)
			} else {
				delegate(w, r)
			}
		}
	}
}

func runHttpsWithAutoRedirect(port string, cert string, key string) {
	if port == "443" {
		go http.ListenAndServe(":80", http.HandlerFunc(redirectHttps))
	}
	runHttpsServer(port, cert, key)
}

func redirectHttps(w http.ResponseWriter, req *http.Request) {
	http.Redirect(w, req,
		"https://"+req.Host+req.URL.String(),
		http.StatusMovedPermanently)
}

func redirectAuth(w http.ResponseWriter, req *http.Request) {
	http.Redirect(w, req,
		schemeAndHost(req)+authConfig.Login.Path,
		http.StatusTemporaryRedirect)
}

func schemeAndHost(r *http.Request) string {
	var scheme string
	if r.TLS != nil {
		scheme = "https://"
	} else {
		scheme = "http://"
	}
	return scheme + r.Host
}

func runHttpsServer(port string, cert string, key string) {
	log.Fatal(http.ListenAndServeTLS(":"+port, cert, key, nil))
}

func runHttpServer(port string) {
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
