package verification

func IsNotEmpty(value string, message string) {
	if len(value) == 0 {
		panic(message)
	}
}
