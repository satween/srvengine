package auth

import (
	"bitbucket.org/satween/srvengine/session"
	"encoding/json"
	"fmt"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"io/ioutil"
	"net/http"
)

const AUTH_PAGE = `<html><body>
<a href="/GoogleLogin">Log in with Google</a>
</body></html>
`

const GoogleOauthRedirectPath = "google_oauth_callback"
const KeyToken = "AccessToken"

const KEY_EMAIL = "Email"

type GoogleProfile struct {
	Email   string
	Picture string
	Name    string
}

var googleOauthConfig *oauth2.Config = nil
var oauthStateString = "random"

//"http://localhost:3000/GoogleCallback",
func Init(redirectUrl string, googleKey string, googleSecret string) {
	googleOauthConfig = &oauth2.Config{
		RedirectURL:  redirectUrl,
		ClientID:     googleKey,
		ClientSecret: googleSecret,
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.profile",
			"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint: google.Endpoint,
	}

	if initAuthDb() != nil {
		panic("Failed to initialize db for auth module")
	}
}

func GoogleAuthHandler(w http.ResponseWriter, r *http.Request) {
	url := googleOauthConfig.AuthCodeURL(oauthStateString)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func GoogleOAuthCallback(w http.ResponseWriter, r *http.Request) {
	state := r.FormValue("state")
	code := r.FormValue("code")

	if state != oauthStateString {
		fmt.Printf("invalid oauth state, expected '%s', got '%s'\n", oauthStateString, state)
		//http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		_, _ = fmt.Fprintf(w, "Failed: \n")
		return
	}

	token, err := googleOauthConfig.Exchange(oauth2.NoContext, code)
	if err != nil {
		fmt.Printf("Code exchange failed with '%s'\n", err)
		//http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		_, _ = fmt.Fprintf(w, "Failed: \n")
		return
	}

	googleProfile, err := loadGoogleProfile(token.AccessToken)
	if err != nil {
		fmt.Printf("Failed to load Google Profile: %s", err)
		_, _ = fmt.Fprintf(w, "Failed \n")
		return
	}

	systemProfile, err := storeProfile(*googleProfile)
	if err != nil {
		fmt.Printf("Failed to store profile: %s", err)
		_, _ = fmt.Fprintf(w, "Failed \n")
		return
	}

	s := session.GetSession()
	s.SetValue(r, KeyToken, &token.AccessToken)
	s.SetProfile(r, systemProfile)
	err = s.Save(r, w)
	if err != nil {
		fmt.Printf("Failed init session: %s", err)
		_, _ = fmt.Fprintf(w, "Failed \n")
		return
	}

	if s.IsExist(r, session.KeyLastUnauthorizedUrl) {
		url := s.GetValue(r, session.KeyLastUnauthorizedUrl).(string)
		s.ClearValue(r, w, session.KeyLastUnauthorizedUrl)
		http.Redirect(w, r, url, http.StatusPermanentRedirect)
		return
	}

	_, _ = fmt.Fprintf(w, "Ok \n")
}

func loadGoogleProfile(token string) (*GoogleProfile, error) {
	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token)
	defer response.Body.Close()

	if err != nil {
		return nil, err
	}

	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	var gProfile GoogleProfile
	_ = json.Unmarshal(contents, &gProfile)

	return &gProfile, nil
}

func Logout(w http.ResponseWriter, r *http.Request) {
	s := session.GetSession()
	s.ClearValue(r, w, KeyToken)

	_, _ = fmt.Fprintf(w, "Logged out\n")
}

func TestToken(w http.ResponseWriter, r *http.Request) {
	s := session.GetSession()
	token := s.GetValue(r, KeyToken)

	_, _ = fmt.Fprintf(w, "Content: %s\n", token)
}
