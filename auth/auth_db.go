package auth

import (
	"bitbucket.org/satween/srvengine/db"
	"bitbucket.org/satween/srvengine/session"
	"fmt"
)

const TableName = "sn_users"

func initAuthDb() error {
	_, err := db.Db.Query(fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s (id serial PRIMARY KEY, email text UNIQUE, picture text, name text);", TableName))
	return err
}

func storeProfile(googleProfile GoogleProfile) (*session.SystemProfile, error) {
	rows := db.Db.QueryRow(fmt.Sprintf("INSERT INTO %s (email, picture, name) VALUES ($1, $2, $3) ON CONFLICT (email) DO UPDATE SET picture = EXCLUDED.picture, name = EXCLUDED.name RETURNING *", TableName), googleProfile.Email, googleProfile.Picture, googleProfile.Name)
	var profile = session.SystemProfile{}
	err := rows.Scan(&profile.Id, &profile.Email, &profile.Picture, &profile.Name)
	if err != nil {
		fmt.Printf("Failed to save data: %s", err)
		return nil, err
	}

	return &profile, nil
}
