package session

import (
	"encoding/gob"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"net/http"
)

const CookieName = "sn_cookie_name"
const KeyUser = "User"
const KeyLastUnauthorizedUrl = "LAST_UNAUTHORIZED_URL"

var _store = sessions.NewCookieStore([]byte(securecookie.GenerateRandomKey(64)))

type SnSession struct {
	store *sessions.CookieStore
}

type SystemProfile struct {
	Id      int64
	Email   string
	Name    string
	Picture string
}

func Setup() {
	gob.Register(SystemProfile{})
}

func (p SnSession) GetProfile(r *http.Request) *SystemProfile {
	session, _ := p.store.Get(r, CookieName)
	if p.IsExist(r, KeyUser) {
		profile := session.Values[KeyUser].(SystemProfile)
		return &profile
	}

	return nil
}

func (p SnSession) SetProfile(r *http.Request, profile *SystemProfile) {
	session, _ := p.store.Get(r, CookieName)
	session.Values[KeyUser] = profile
}

func (p SnSession) ClearValue(r *http.Request, w http.ResponseWriter, key string) {
	session, _ := p.store.Get(r, CookieName)
	delete(session.Values, key)
	_ = session.Save(r, w)
}

func (p SnSession) SetValue(r *http.Request, key string, value interface{}) {
	session, _ := p.store.Get(r, CookieName)
	session.Values[key] = value
}

func (p SnSession) IsExist(r *http.Request, key string) bool {
	session, _ := p.store.Get(r, CookieName)
	_, ok := session.Values[key]

	return ok
}

func (p SnSession) GetValue(r *http.Request, key string) interface{} {
	session, _ := p.store.Get(r, CookieName)
	return session.Values[key]
}

func (p SnSession) Save(r *http.Request, w http.ResponseWriter) error {
	session, _ := p.store.Get(r, CookieName)
	return session.Save(r, w)
}

func GetSession() *SnSession {
	return &SnSession{store: _store}
}
