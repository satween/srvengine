```go
package main

import (
	"./auth"
	"./db"
	"./server"
	"fmt"
	"net/http"
)

func main() {


	handlers := []server.RequestHandler{
		{
			Path:        "/test",
			HandlerFunc: server.AuthorizationRequired(LoadHandler),
		},
	}

	authConfig := server.AuthConfig{

		Login: server.RequestHandler{
			Path:        "/auth",
			HandlerFunc: auth.GoogleAuthHandler,
		},

		Logout: server.RequestHandler{
			Path:        "/logout",
			HandlerFunc: auth.Logout,
		},
		GoogleKey:"...",
		GoogleSecret:"...",
	}

	db.Init("test", "test", "test")


	var config = server.Configuration{
		Handlers: handlers,
		Port:     "8080",
		Auth:     &authConfig,
	}

	server.Run(config)
}

func LoadHandler(w http.ResponseWriter, r *http.Request) {
	_, _ = fmt.Fprintf(w, "Ok: \n")
}

```