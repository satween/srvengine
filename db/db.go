package db

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

type DBConfig struct {
	user     string
	password string
	dbName   string
}

var Config DBConfig
var Db *sql.DB = nil

func Init(dbName string, user string, password string) {
	Config = DBConfig{
		user:     user,
		password: password,
		dbName:   dbName}

	db, err := sql.Open("postgres", fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", user, password, dbName))

	Db = db

	if err == nil {
		err = Db.Ping()
	}

	if err != nil {
		panic("Failed to connect to db")
	}
}
